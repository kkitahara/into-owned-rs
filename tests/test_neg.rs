// Copyright 2020 Koichi Kitahara
//
// Licensed under either of Apache License, Version 2.0:
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// or MIT license:
//
//   Permission is hereby granted, free of charge, to any person obtaining a
//   copy of this software and associated documentation files (the "Software"),
//   to deal in the Software without restriction, including without limitation
//   the rights to use, copy, modify, merge, publish, distribute, sublicense,
//   and/or sell copies of the Software, and to permit persons to whom the
//   Software is furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in
//   all copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
//   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//   DEALINGS IN THE SOFTWARE.
//
// at your option.

use into_owned::IntoOwned;

#[test]
fn test_neg() {
    use core::ops::Neg;

    fn call_neg_as_is<T, Out>(x: T) -> Out
    where
        T: IntoOwned,
        T::Owned: Neg<Output = Out>,
        for<'a> &'a T::Owned: Neg<Output = Out>,
    {
        let y = x.as_is();
        -y
    }

    fn call_neg_into_gyu<T, Out>(x: T) -> Out
    where
        T: IntoOwned,
        T::Owned: Neg<Output = Out>,
        for<'a> &'a T::Owned: Neg<Output = Out>,
    {
        let y = x.into_gyu();
        -y
    }

    fn call_neg_into_gyu_mut<T, Out>(x: T) -> Out
    where
        T: IntoOwned,
        T::Owned: Clone + Neg<Output = Out>,
        for<'a> &'a T::Owned: Neg<Output = Out>,
    {
        let y = x.into_gyu_mut();
        -y
    }

    macro_rules! call_neg {
        ($lhs:expr, $ret:expr) => {
            assert_eq!(call_neg_as_is($lhs), $ret);
            assert_eq!(call_neg_into_gyu($lhs), $ret);
            assert_eq!(call_neg_into_gyu_mut($lhs), $ret);
        };
    }

    call_neg!(1.0, -1.0);
    call_neg!(&1.0, -1.0);
    call_neg!(&mut 1.0, -1.0);
    call_neg!(1.0.as_is(), -1.0);
    call_neg!((&1.0).as_is(), -1.0);
    call_neg!((&mut 1.0).as_is(), -1.0);
    call_neg!(1.0.into_gyu(), -1.0);
    call_neg!((&1.0).into_gyu(), -1.0);
    call_neg!((&mut 1.0).into_gyu(), -1.0);
    call_neg!(1.0.into_gyu_mut(), -1.0);
    call_neg!((&1.0).into_gyu_mut(), -1.0);
    call_neg!((&mut 1.0).into_gyu_mut(), -1.0);
}
