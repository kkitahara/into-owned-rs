// Copyright 2020 Koichi Kitahara
//
// Licensed under either of Apache License, Version 2.0:
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// or MIT license:
//
//   Permission is hereby granted, free of charge, to any person obtaining a
//   copy of this software and associated documentation files (the "Software"),
//   to deal in the Software without restriction, including without limitation
//   the rights to use, copy, modify, merge, publish, distribute, sublicense,
//   and/or sell copies of the Software, and to permit persons to whom the
//   Software is furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in
//   all copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
//   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//   DEALINGS IN THE SOFTWARE.
//
// at your option.

use into_owned::{IntoOwned, Owned};

#[test]
fn test_add() {
    use core::ops::Add;

    fn call_add_as_is_as_is<T, U, Out>(x: T, y: U) -> Out
    where
        T: IntoOwned,
        U: IntoOwned,
        for<'a> T::Owned: Owned + Add<U::Owned, Output = Out> + Add<&'a U::Owned, Output = Out>,
        for<'a, 'b> &'a T::Owned: Add<U::Owned, Output = Out> + Add<&'b U::Owned, Output = Out>,
    {
        let x = x.as_is();
        let y = y.as_is();
        x + y
    }

    fn call_add_as_is_into_gyu<T, U, Out>(x: T, y: U) -> Out
    where
        T: IntoOwned,
        U: IntoOwned,
        for<'a> T::Owned: Owned + Add<U::Owned, Output = Out> + Add<&'a U::Owned, Output = Out>,
        for<'a, 'b> &'a T::Owned: Add<U::Owned, Output = Out> + Add<&'b U::Owned, Output = Out>,
    {
        let x = x.as_is();
        let y = y.into_gyu();
        x + y
    }

    fn call_add_as_is_into_gyu_mut<T, U, Out>(x: T, y: U) -> Out
    where
        T: IntoOwned,
        U: IntoOwned,
        for<'a> T::Owned: Owned + Add<U::Owned, Output = Out> + Add<&'a U::Owned, Output = Out>,
        for<'a, 'b> &'a T::Owned: Add<U::Owned, Output = Out> + Add<&'b U::Owned, Output = Out>,
        U::Owned: Clone,
    {
        let x = x.as_is();
        let y = y.into_gyu_mut();
        x + y
    }

    fn call_add_into_gyu_as_is<T, U, Out>(x: T, y: U) -> Out
    where
        T: IntoOwned,
        U: IntoOwned,
        for<'a> T::Owned: Owned + Add<U::Owned, Output = Out> + Add<&'a U::Owned, Output = Out>,
        for<'a, 'b> &'a T::Owned: Add<U::Owned, Output = Out> + Add<&'b U::Owned, Output = Out>,
    {
        let x = x.into_gyu();
        let y = y.as_is();
        x + y
    }

    fn call_add_into_gyu_into_gyu<T, U, Out>(x: T, y: U) -> Out
    where
        T: IntoOwned,
        U: IntoOwned,
        for<'a> T::Owned: Owned + Add<U::Owned, Output = Out> + Add<&'a U::Owned, Output = Out>,
        for<'a, 'b> &'a T::Owned: Add<U::Owned, Output = Out> + Add<&'b U::Owned, Output = Out>,
    {
        let x = x.into_gyu();
        let y = y.into_gyu();
        x + y
    }

    fn call_add_into_gyu_into_gyu_mut<T, U, Out>(x: T, y: U) -> Out
    where
        T: IntoOwned,
        U: IntoOwned,
        for<'a> T::Owned: Owned + Add<U::Owned, Output = Out> + Add<&'a U::Owned, Output = Out>,
        for<'a, 'b> &'a T::Owned: Add<U::Owned, Output = Out> + Add<&'b U::Owned, Output = Out>,
        U::Owned: Clone,
    {
        let x = x.into_gyu();
        let y = y.into_gyu_mut();
        x + y
    }

    fn call_add_into_gyu_mut_as_is<T, U, Out>(x: T, y: U) -> Out
    where
        T: IntoOwned,
        U: IntoOwned,
        for<'a> T::Owned:
            Owned + Clone + Add<U::Owned, Output = Out> + Add<&'a U::Owned, Output = Out>,
        for<'a, 'b> &'a T::Owned: Add<U::Owned, Output = Out> + Add<&'b U::Owned, Output = Out>,
    {
        let x = x.into_gyu_mut();
        let y = y.as_is();
        x + y
    }

    fn call_add_into_gyu_mut_into_gyu<T, U, Out>(x: T, y: U) -> Out
    where
        T: IntoOwned,
        U: IntoOwned,
        for<'a> T::Owned:
            Owned + Clone + Add<U::Owned, Output = Out> + Add<&'a U::Owned, Output = Out>,
        for<'a, 'b> &'a T::Owned: Add<U::Owned, Output = Out> + Add<&'b U::Owned, Output = Out>,
    {
        let x = x.into_gyu_mut();
        let y = y.into_gyu();
        x + y
    }

    fn call_add_into_gyu_mut_into_gyu_mut<T, U, Out>(x: T, y: U) -> Out
    where
        T: IntoOwned,
        U: IntoOwned,
        for<'a> T::Owned:
            Owned + Clone + Add<U::Owned, Output = Out> + Add<&'a U::Owned, Output = Out>,
        for<'a, 'b> &'a T::Owned: Add<U::Owned, Output = Out> + Add<&'b U::Owned, Output = Out>,
        U::Owned: Clone,
    {
        let x = x.into_gyu_mut();
        let y = y.into_gyu_mut();
        x + y
    }

    macro_rules! call_add {
        ($lhs:expr, $rhs:expr, $ret:expr) => {
            assert_eq!(call_add_as_is_as_is($lhs, $rhs), $ret);
            assert_eq!(call_add_as_is_into_gyu($lhs, $rhs), $ret);
            assert_eq!(call_add_as_is_into_gyu_mut($lhs, $rhs), $ret);
            assert_eq!(call_add_into_gyu_as_is($lhs, $rhs), $ret);
            assert_eq!(call_add_into_gyu_into_gyu($lhs, $rhs), $ret);
            assert_eq!(call_add_into_gyu_into_gyu_mut($lhs, $rhs), $ret);
            assert_eq!(call_add_into_gyu_mut_as_is($lhs, $rhs), $ret);
            assert_eq!(call_add_into_gyu_mut_into_gyu($lhs, $rhs), $ret);
            assert_eq!(call_add_into_gyu_mut_into_gyu_mut($lhs, $rhs), $ret);
        };
    }

    call_add!(1.0, 1.0, 2.0);
    call_add!(1.0, &1.0, 2.0);
    call_add!(1.0, &mut 1.0, 2.0);
    call_add!(&1.0, 1.0, 2.0);
    call_add!(&1.0, &1.0, 2.0);
    call_add!(&1.0, &mut 1.0, 2.0);
    call_add!(&mut 1.0, 1.0, 2.0);
    call_add!(&mut 1.0, &1.0, 2.0);
    call_add!(&mut 1.0, &mut 1.0, 2.0);
}
